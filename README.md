# my_first_package

Тестовый репозиторий для сборки собственного пакета

Пакет собирался по мануалу: https://packaging.python.org/en/latest/tutorials/packaging-projects/

### 1. Как собрать .whl

Предварительно нужно установить poetry: https://python-poetry.org/docs/

Склонировать репозиторий и в его корне выполнить:

    python -m build

### 2. Как установить пакет

В корне репозитория выполнить:

    pip install dist/my_first_package-0.0.1-py3-none-any.whl

### 3. Проверить, что глобальный интерпретатор видит пакет:

    pip list | grep my-first-package

### 4. Проверить, что пакет импортируется и функции отрабатывают

``` python
from my_first_package import useless_tools


print(useless_tools.get_dative_by_name('Олег'))

# output: Олегу
```

## Установить пакет можно и напрямую через GitLab

    pip install git+https://{token_username}:{generated_token}@gitlab.com/anton.zakladniy/my_first_package.git


Где token_username и generated_token - секретный токен для чтения из конкретного репозитория