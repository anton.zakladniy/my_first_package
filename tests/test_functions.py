"""Модуль с тестами."""
import pytest

from src.my_first_package import useless_tools


@pytest.mark.parametrize("test_input, expected", [(5, 6), (5, 5)])
def test_function(test_input, expected) -> None:
    """Тесты для функции, добавляющей случайное число."""
    assert useless_tools.add_random_value(test_input) != expected


@pytest.mark.parametrize("test_input, expected", [('Иван', 'Ивану'), ('Павел', 'Павелу')])
def test_function(test_input, expected) -> None:
    """Тесты для функции, возвращаещей имя в дательном падеже."""
    assert useless_tools.get_dative_by_name(test_input) == expected