"""Модуль с тестовыми функциями.

Здесь приведены тестовые функции для пробной реализации пакета.
"""
import random

from pytrovich.enums import NamePart, Gender, Case
from pytrovich.maker import PetrovichDeclinationMaker

maker = PetrovichDeclinationMaker()


def test_func(number: float) -> float:
    """Добавляет случайное число к заданному.

    :param number: входящее число
    :return: вхоядщее число + случайное
    """
    return number + random.random()


def get_dative_by_name(name: str, gender: str = 'male') -> str:
    """Возвращает имя человека в дательном падеже.
    :param gender: пол человека
    :param name: имя человека
    :return: имя человека в дательном падеже
    """
    if gender == 'female':
        gender = Gender.FEMALE
    elif gender == 'male':
        gender = Gender.MALE
    else:
        gender = Gender.MALE
    return maker.make(NamePart.FIRSTNAME, gender, Case.DATIVE, name)

